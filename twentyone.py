import random, socket


class Card():
    def __init__(self, color, name, value):
        self.color = color
        self.name = name
        self.value = value


class Pack():
    def __init__(self):
        color_list = ["\u2660", "\u2663", "\u2665", "\u2666"]
        name_list = {"6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "V": 2, "D": 3, "K": 4, "T": 11}

        self.pack = []

        for color in color_list:
            for name in name_list:
                self.pack.append(Card(color, name, name_list[name]))

    def pack_shuffle(self):
        random.shuffle(self.pack)

    def take_card(self):
        return self.pack.pop(0)


class Hand():
    def __init__(self):
        self.current_sum = 0

    def take_card(self, pack):
        current_card = pack.take_card()

        if current_card.name == "T":
            if self.current_sum > 10:
                self.current_sum += current_card.value - 10
            else:
                self.current_sum += current_card.value
        else:
            self.current_sum += current_card.value

        return current_card.color + current_card.name


class Game():
    def __init__(self):
        self.pack = Pack()
        self.pack.pack_shuffle()

        self.gamer = Hand()
        self.dealer = Hand()

        self.first_hand = True
        self.user_choice = None

        self.response = ''

    def game_process(self):
        if self.first_hand is True:
            self.first_hand = False
            first_hand_gamer = (self.gamer.take_card(self.pack), self.gamer.take_card(self.pack))
            first_hand_dealer = (self.dealer.take_card(self.pack), self.dealer.take_card(self.pack))

            self.response += "============ Начало игры ===============\n"

            self.response += "Вам раздали %s и %s\n" % first_hand_gamer
            self.response += "Дилер взял %s и %s\n" % first_hand_dealer

            if first_hand_gamer[0][1:] == 'T' and first_hand_gamer[1][1:] == 'T' and \
               first_hand_dealer[0][1:] == 'T' and first_hand_dealer[1][1:] == 'T':
                self.response += "Дилер выиграл!\n"
                return self.response

            elif first_hand_gamer[0][1:] == first_hand_gamer[1][1:] and first_hand_gamer[0][1:] == 'T':
                self.response += "Вы выиграли! Поздравляем!\n"
                return self.response

            elif first_hand_dealer[0][1:] == first_hand_dealer[1][1:] and first_hand_dealer[0][1:] == 'T':
                self.response += "Дилер выиграл!\n"
                return self.response
            else:
                self.response += "Ваши текущие очки: %i\n" % self.gamer.current_sum
                self.response += "Текущие очки дилера: %i\n" % self.dealer.current_sum

        #self.response += "=========================================\n"

        if self.gamer.current_sum > 21 and self.dealer.current_sum > 21:
            self.response += "Дилер выиграл!\n"
            return self.response

        elif self.gamer.current_sum > 21 and self.dealer.current_sum <= 21:
            self.response += "Дилер выиграл!\n"
            return self.response

        elif self.gamer.current_sum <= 21 and self.dealer.current_sum > 21:
            self.response += "Вы выиграли! Поздравляем!"
            return self.response

        elif self.gamer.current_sum <= 21 and self.dealer.current_sum <= 21:

            if self.user_choice is False:
                while True:
                    if self.dealer.current_sum < 18:
                        self.response += "Дилер взял: %s\n" % self.dealer.take_card(self.pack)
                        self.response += "Текущие очки дилера: %i\n" % self.dealer.current_sum
                    else:
                        break

                if self.gamer.current_sum < self.dealer.current_sum and self.dealer.current_sum <= 21:
                    self.response += "Дилер выиграл!\n"
                    return self.response
                elif self.gamer.current_sum == self.dealer.current_sum and self.dealer.current_sum <= 21:
                    self.response += "Дилер выиграл!\n"
                    return self.response
                else:
                    self.response += "Вы выиграли! Поздравляем!"
                    return self.response
            elif self.user_choice is True:
                self.response += "Вы взяли %s\n" % self.gamer.take_card(self.pack)
                if self.dealer.current_sum < 18:
                    self.response += "Дилер взял %s\n" % self.dealer.take_card(self.pack)
                else:
                    self.response += "Дилер больше не хочет брать карту\n"

                self.response += "Ваши текущие очки: %i\n" % self.gamer.current_sum
                self.response += "Текущие очки дилера: %i\n" % self.dealer.current_sum

                if self.gamer.current_sum > 21 and self.dealer.current_sum > 21:
                    self.response += "Дилер выиграл!\n"
                    return self.response

                elif self.gamer.current_sum > 21 and self.dealer.current_sum <= 21:
                    self.response += "Дилер выиграл!\n"
                    return self.response

                elif self.gamer.current_sum <= 21 and self.dealer.current_sum > 21:
                    self.response += "Вы выиграли! Поздравляем!"
                    return self.response

                return self.response
            else:
                return self.response




#Start game
HOST = "127.0.0.1"  # localhost
PORT = 33333
srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
srv.bind((HOST, PORT))

while 1:
    print("Слушаю порт 33333")
    srv.listen(1)
    sock, addr = srv.accept()
    client_message = sock.recv(1024)
    print("Получено от %s:%s:" % addr,client_message.decode())
    if client_message.decode() == 'n':
        a.user_choice = False
    elif client_message.decode() == 'y':
        a.user_choice = True
    elif client_message.decode() == 'Start': # если новая игра
        a = Game()
        a.user_choice = None
        a.first_hand = True
    response = a.game_process()
    print("Отправлено %s:%s:" % addr, response)
    sock.send(response.encode())
    sock.close()
    a.response = ''