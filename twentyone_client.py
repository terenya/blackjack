import socket
HOST = "127.0.0.1" # удаленный компьютер (localhost)
PORT = 33333 # порт на удаленном компьютере
while 1:
    a = input("Желаете начать новую игру? (наберите y или n и нажмите ввод)")
    if a =='n':
        print("Всего доброго!")
        break
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
    sock.send(b"Start")
    result = sock.recv(1024)
    if 'Вы выиграли! Поздравляем!' in result.decode() or 'Дилер выиграл!' in result.decode():
        print("=========== Конец игры ===========")
        break
    sock.close()
    print(result.decode())
  
    while 1:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
        a = input("Хотите взять ещё одну карту? (наберите y или n и нажмите ввод)")
        sock.send(a.encode())
        result = sock.recv(1024)
        sock.close()
        print(result.decode())
        if 'Вы выиграли! Поздравляем!' in result.decode() or 'Дилер выиграл!' in result.decode():
            print("=========== Конец игры ===========")
            break